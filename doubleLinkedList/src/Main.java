/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dangn
 */
public class Main {
    public static void main(String[] args) {
        doubleLinkedList mylist = new doubleLinkedList();
        System.out.println("Test addhead");
        mylist.addFirst(3);
        mylist.addFirst(15);
        mylist.addFirst(-6);
        mylist.addFirst(4);
        mylist.addLast(4);
        mylist.addLast(5);
        System.out.println("\nFrom Head -> Tail");
        mylist.traverse1();
        System.out.println("\nFrom Tail -> Head");
        mylist.traverse2();
        Object x = 4;
        System.out.println("\nTest remove first value " + x);
        mylist.removeFirstValueX(x);
        System.out.println("\nFrom Head -> Tail");
        mylist.traverse1();
    }
}
