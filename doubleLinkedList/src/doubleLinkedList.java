/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dangn
 */
public class doubleLinkedList {
    Node head, tail;
    boolean isEmpty(){
        
        return head == null;
    }
    //buoc 1: cap phat Node moi chua inf la x Node p = new Node(x);
    void addFirst(Object x){
        Node p = new Node(x);
        if(isEmpty()){
            head = tail = p;
        }else{
            p.next = head;
            head.prev = p;
            head = p; // = head.prev;
        }
    }
    void addLast(Object x){
        Node p = new Node(x);
        if(isEmpty()){
            head = tail = p;
        }else{
            tail.next = p;
            p.prev = tail;
            tail = p;
        }
    }
    //Tim vi tri cua Node co gia tri bang 7
    void removeFirstValueX(Object x){
        Node p = head;
        while(p != null && p.inf != x){
            p = p.next;
        }
        if(p == null){
    return;
}
        if(p == head){
            head = head.next;
            head.prev = null;
        }else if(p == tail){
            tail = tail.prev;
            tail.next = null;
        }else{
            p.prev.next = p.next;
            p.next.prev = p.prev;
        }
    }
    void traverse1(){
        Node p = head;
        while(p != null){
            System.out.print(p.inf + " ");
            p = p.next;
        }
        System.out.println("");
    }
    void traverse2(){
        Node p = tail;
        while (p != null){
            System.out.print(p.inf + " ");
            p = p.prev;
        }
        System.out.println("");
    }
    }

