/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dangn
 */
public class Node {
    Object inf;
    Node next,prev;

    public Node() {
    }

    public Node(Object inf, Node next, Node prev) {
        this.inf = inf;
        this.next = next;
        this.prev = prev;
    }
    
    public Node(Object inf){
        this(inf,null,null);
    }
}
